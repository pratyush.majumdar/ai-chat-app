# AI Chatbot
An AI chatbot created using the following Libraries and Tools
1. Python
2. LangChain
3. OpenAI APIs
4. Pinecone Vector Database

## Installation
```python
pip3 install virtualenv
python3 -m virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt
python3 api/app.py
```

## Install Vercel CLI for Node
```javascript
npm i -g vercel
```

## Build and deploy new changes to Vercel
```python
vercel .
vercel --prod
```

## Test from Browser
https://ai-chat-app-five.vercel.app