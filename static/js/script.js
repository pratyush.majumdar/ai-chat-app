$(document).ready(function() {
    $("form").on("submit", function(event) {
        $('#send').attr("disabled", true);
        $('#loader').show();
        var rawText = $("#text").val();
        var userHtml = '<p class="userText"><span>You: ' + rawText + "</span></p>";
        $("#text").val("");
        $("#chatbox").append(userHtml);
        document.getElementById("userInput").scrollIntoView({
            block: "start",
            behavior: "smooth",
        });
        $.ajax({
            data: {
                msg: rawText,
            },
            type: "POST",
            url: "/get",
        }).done(function(data) {
            $('#loader').hide();
            var botHtml = '<p class="botText"><span>ChatBot: ' + data + "</span></p>";
            $("#chatbox").append($.parseHTML(botHtml));
            document.getElementById("userInput").scrollIntoView({
                block: "start",
                behavior: "smooth",
            });
            $('#send').attr("disabled", false);
        });
        event.preventDefault();
    });
});
