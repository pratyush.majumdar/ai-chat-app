from flask import Flask, render_template, request
import openai
import os
from dotenv import load_dotenv

app = Flask(__name__, template_folder="../templates", static_folder="../static")

load_dotenv()
openai.api_key = os.getenv('OPENAI_API_KEY')

def get_completion(prompt):
    print(prompt)
    query = openai.Completion.create(
        engine="text-davinci-003",
        prompt=prompt,
        max_tokens=1024,
        n=1,
        stop=None,
        temperature=0.5,
    )
  
    response = query.choices[0].text
#    print(response)
    return response

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/get", methods=["POST"])
def chatbot_response():
    prompt = request.form["msg"]
    response = get_completion(prompt)
    return response

if __name__ == "__main__":
    app.run(debug=True)