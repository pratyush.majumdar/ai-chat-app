import os
import openai
import pinecone
from dotenv import load_dotenv
from flask import Flask, render_template, request

load_dotenv()
openai.api_key = os.getenv('OPENAI_API_KEY')

# Get Pinecone API environment variables
pinecone_api = os.getenv('PINECONE_API_KEY')
pinecone_env = os.getenv('PINECONE_API_ENV')
pinecone_index = os.getenv('PINECONE_INDEX')

app = Flask(__name__, template_folder="../templates", static_folder="../static")

def get_completion(query):
    # Initialize Pinecone client and set index
    pinecone.init(api_key=pinecone_api, environment=pinecone_env)
    index = pinecone.Index(pinecone_index)

    # Convert your query into a vector using OpenAI text embedding
    try:
        query_vector = openai.Embedding.create(
            input=query,
            engine='text-embedding-ada-002',
        )["data"][0]["embedding"]
    except Exception as e:
        print(e)

    # Search for the 3 most similar vectors in Pinecone
    search_response = index.query(
        top_k=3,
        vector=query_vector,
        include_metadata=True)

    # Combine the 3 vectors into a single text variable that it will be added in the prompt
    chunks = [item["metadata"]['text'] for item in search_response['matches']]
    joined_chunks = "\n".join(chunks)

# Being a gadget expert answer the following question with concrete and structured answer based on the context below.
    try:
        # Build the prompt
        prompt = f"""
        Answer the following question based on the context below.
        If you don't know the answer, just say that you don't know. Don't try to make up an answer. Do not answer beyond this context.
        ---
        QUESTION: {query}                                            
        ---
        CONTEXT:
        {joined_chunks}
        """

        # Run chat completion using GPT-3.5
        response = openai.ChatCompletion.create(
            model='gpt-3.5-turbo',
            messages=[
                { "role": "system", "content":  "You are a Q&A assistant." },
                { "role": "user", "content": prompt }
            ],
            temperature=0.7,
            max_tokens=1000
        )
        result = response.choices[0]['message']['content']
    except Exception as e:
        print(e)
    print(result)
    return result

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/get", methods=["POST"])
def chatbot_response():
    question = request.form["msg"]
    print(question)
    response = get_completion(question)
    return response

if __name__ == "__main__":
    app.run(debug=True)